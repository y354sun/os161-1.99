#include <types.h>
#include <kern/errno.h>
#include <kern/unistd.h>
#include <kern/fcntl.h>
#include <kern/wait.h>
#include <lib.h>
#include <syscall.h>
#include <current.h>
#include <proc.h>
#include <thread.h>
#include <addrspace.h>
#include <copyinout.h>
#include <mips/trapframe.h>
#include <limits.h>
#include <vm.h>
#include <vfs.h>
#include <test.h>

#include "opt-A2.h"

  /* this implementation of sys__exit does not do anything with the exit code */
  /* this needs to be fixed to get exit() and waitpid() working properly */

void sys__exit(int exitcode) {

  struct addrspace *as;
  struct proc *p = curproc;
  /* for now, just include this to keep the compiler from complaining about
     an unused variable */
  (void)exitcode;

  DEBUG(DB_SYSCALL,"Syscall: _exit(%d)\n",exitcode);

  KASSERT(curproc->p_addrspace != NULL);
  as_deactivate();
  /*
   * clear p_addrspace before calling as_destroy. Otherwise if
   * as_destroy sleeps (which is quite possible) when we
   * come back we'll be calling as_activate on a
   * half-destroyed address space. This tends to be
   * messily fatal.
   */
  as = curproc_setas(NULL);
  as_destroy(as);

  /* detach this thread from its process */
  /* note: curproc cannot be used after this call */
  proc_remthread(curthread);

  #if OPT_A2
  struct proc_record *pr = precord_search(p->pId);

  if (pr->parentId != INVALID) {
      pr->exitState = ZOMBIE;
      pr->exitCode = _MKWAIT_EXIT(exitcode);

      lock_acquire(proc_pidLock);
      cv_broadcast(proc_pidCV, proc_pidLock);
      lock_release(proc_pidLock);
  } else {
      proc_usedTable[pr->pId-PID_MIN] = false;
      pr->exitState = EXITED;
  }

  #endif
  
  /* if this is the last user process in the system, proc_destroy()
     will wake up the kernel menu thread */
  proc_destroy(p);
  
  thread_exit();
  /* thread_exit() does not return, so we should never get here */
  panic("return from thread_exit in sys_exit\n");
}


/* stub handler for getpid() system call                */
int
sys_getpid(pid_t *retval)
{
  #if OPT_A2
    KASSERT(curproc != NULL);
    *retval = curproc->pId;
  #else
  /* for now, this is just a stub that always returns a PID of 1 */
  /* you need to fix this to make it work properly */
  *retval = 1;
  #endif
  return(0);
}

/* stub handler for waitpid() system call                */

int
sys_waitpid(pid_t pid,
	    userptr_t status,
	    int options,
	    pid_t *retval)
{
  int exitstatus;
  int result;

  /* this is just a stub implementation that always reports an
     exit status of 0, regardless of the actual exit status of
     the specified process.   
     In fact, this will return 0 even if the specified process
     is still running, and even if it never existed in the first place.

     Fix this!
  */

  if (options != 0) {
    return(EINVAL);
  }

  #if OPT_A2
    struct proc_record *childr = precord_search(pid);

    if (childr == NULL) {
      *retval = -1;
      return ESRCH;
    }
    if (childr->parentId != curproc->pId) {
      *retval = -1;
      return ECHILD;
    }

    lock_acquire(proc_pidLock);

    while (childr->exitState == ACTIVE) {
      cv_wait(proc_pidCV, proc_pidLock);
    }

    exitstatus = childr->exitCode;
    lock_release(proc_pidLock);

  #else
  /* for now, just pretend the exitstatus is 0 */
  exitstatus = 0;
  #endif

  result = copyout((void *)&exitstatus,status,sizeof(int));
  if (result) {
    return(result);
  }
  *retval = pid;
  return(0);
}

#if OPT_A2
int 
sys_fork(struct trapframe *tf, pid_t *retval)
{
  KASSERT(curproc != NULL);
  int result;

  struct proc *new_proc = proc_create_runprogram(curproc->p_name);
  struct proc_record *new_pr = precord_search(new_proc->pId);
  new_pr->parentId = curproc->pId;

  if (new_proc == NULL || new_pr == NULL) {
    return ENOMEM;
  }
  
  if (new_proc->pId == INVALID) {
    proc_destroy(new_proc);
    return EMPROC;
  }
  
  // Address space
  spinlock_acquire(&new_proc->p_lock);
  result = as_copy(curproc_getas(), &(new_proc->p_addrspace));
  spinlock_release(&new_proc->p_lock);

  if (result || new_proc->p_addrspace == NULL) {
    DEBUG(DB_SYSCALL, "sys_fork ERROR: could not create new address space");
    proc_destroy(new_proc);
    return ENOMEM;
  }


  // Trap frame
  struct trapframe *new_tf = kmalloc(sizeof(struct trapframe));
  if (new_tf == NULL) {
    DEBUG(DB_SYSCALL, "sys_fork ERROR: could not create new trapframe space");
    proc_destroy(new_proc);
    return ENOMEM;
  }
  memcpy(new_tf, tf, sizeof(struct trapframe));


  // Thread
  result = thread_fork(curthread->t_name, new_proc, 
                      (void *)enter_forked_process, new_tf, 0);
  if (result) {
    kfree(new_tf);
    proc_destroy(new_proc);
    DEBUG(DB_SYSCALL, "sys_fork ERROR: thread_fork failed");
    return ENOMEM;
  }

  // Set return value
  *retval = new_proc->pId;
  return 0;
}

static void argv_destroy(int argc, char **argv) {
  for (int i = 0; i < argc; ++i) {
    kfree(argv[i]);    
  }
  kfree(argv);
}

int 
sys_execv(const_userptr_t program, userptr_t args)
{
  int result;
  size_t prog_len;
  size_t arg_len;

  /* 
  Copy the program path into the kernel 
  */

  char *prog_kcopy = kmalloc(PATH_MAX);
  if (prog_kcopy == NULL) {
    return ENOMEM;
  }

  result = copyinstr(program, prog_kcopy, PATH_MAX, &prog_len);
  if (result) {
      DEBUG(DB_SYSCALL, "sys_execv ERROR: could not copy program path");
      return result;
  }

  if (prog_len == 1) {
    return ENOENT;            /* No such file or directory */
  }
  
  /* 
  Count the number of arguments and copy them into the kernel 
  */
  unsigned int args_count = 0;

  while (((char**)args)[args_count] != NULL) {
    ++ args_count;
  }

  if (args_count > ARG_MAX) {
      return E2BIG;
  }

  char **args_kcopy = kmalloc((args_count+1) * sizeof(char *));
  if (args_kcopy == NULL) {
    return ENOMEM;
  }

  result = copyin(args, args_kcopy, (args_count+1)*sizeof(char *));
  if (result) {
      DEBUG(DB_SYSCALL, "sys_execv ERROR: could not copy args ");
      return result;
  }

  for (unsigned int j = 0; j < args_count; ++ j) {
    arg_len = strlen(((char **)args)[j])+1;
    args_kcopy[j] = kmalloc(arg_len);

    result = copyinstr(((userptr_t *)args)[j], args_kcopy[j], arg_len, NULL);
    if (result) {
      DEBUG(DB_SYSCALL, "sys_execv ERROR: could not copy args ");
      return result;
    }
  }

  args_kcopy[args_count] = NULL;

  // call runprogram
  result = runprogram(prog_kcopy, args_count, args_kcopy);
  if (result) {
    kfree(prog_kcopy);
    argv_destroy(args_count, args_kcopy);

    return result;
  }

  return 0;
}

#endif
