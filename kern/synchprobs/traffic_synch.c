#include <types.h>
#include <lib.h>
#include <synchprobs.h>
#include <synch.h>
#include <array.h>
#include <opt-A1.h>

/* 
 * This simple default synchronization mechanism allows only vehicle at a time
 * into the intersection.   The intersectionSem is used as a a lock.
 * We use a semaphore rather than a lock so that this code will work even
 * before locks are implemented.
 */

/* 
 * Replace this default synchronization mechanism with your own (better) mechanism
 * needed for your solution.   Your mechanism may use any of the available synchronzation
 * primitives, e.g., semaphores, locks, condition variables.   You are also free to 
 * declare other global variables if your solution requires them.
 */

/*
 * replace this with declarations of any synchronization and other variables you need here
 */

// lock
static struct lock *intersectionLk;
// cv
static struct cv *StoN;
static struct cv *WtoN;
static struct cv *EtoN;
static struct cv *NtoS;
static struct cv *EtoS;
static struct cv *WtoS;
static struct cv *StoW;
static struct cv *NtoW;
static struct cv *EtoW;
static struct cv *StoE;
static struct cv *NtoE;
static struct cv *WtoE;
// array
struct array *vehicles;

typedef struct Vehicle
{
  Direction origin;
  Direction destination;
} Vehicle;

bool right_turn(Vehicle *v);
Vehicle* vehicle_create(Direction origin, Direction destination);
bool detect_collisions(Vehicle *v1, Vehicle *v2);
bool enter_or_not(Vehicle *newVec);

bool right_turn(Vehicle *v) {
  KASSERT(v != NULL);
  if (((v->origin == west) && (v->destination == south)) ||
      ((v->origin == south) && (v->destination == east)) ||
      ((v->origin == east) && (v->destination == north)) ||
      ((v->origin == north) && (v->destination == west))) {
      return true;
  } else {
      return false;
  }
}



/* 
 * The simulation driver will call this function once before starting
 * the simulation
 *
 * You can use it to initialize synchronization and other variables.
 * 
 */
void
intersection_sync_init(void)
{
  intersectionLk = lock_create("intersectionLk");
  if (intersectionLk == NULL) {
      	panic("could not create intersection lock");
  }

  StoE = cv_create("StoE");
  StoW = cv_create("StoW");
  StoN = cv_create("StoN");
  WtoE = cv_create("WtoE");
  WtoS = cv_create("WtoS");
  WtoN = cv_create("WtoN");
  EtoS = cv_create("EtoS");
  EtoW = cv_create("EtoW");
  EtoN = cv_create("EtoN");
  NtoS = cv_create("NtoS");
  NtoW = cv_create("NtoW");
  NtoE = cv_create("NtoE");

  if (WtoN == NULL || WtoE == NULL || NtoE == NULL || StoE == NULL || StoN == NULL || StoW == NULL ||
      WtoS == NULL || EtoS == NULL || EtoN == NULL || EtoW == NULL || NtoS == NULL || NtoW == NULL) {
      	panic("could not create intersection cv");
  }
  
  vehicles = array_create();
  if (vehicles == NULL) {
      	panic("could not create an array of Vehicles at the intersection");
  }

  return;
}

/* 
 * The simulation driver will call this function once after
 * the simulation has finished
 *
 * You can use it to clean up any synchronization and other variables.
 *
 */
void
intersection_sync_cleanup(void)
{
  KASSERT(vehicles != NULL && intersectionLk != NULL);
  KASSERT(!(WtoN == NULL || WtoE == NULL || NtoE == NULL || StoE == NULL || StoN == NULL || StoW == NULL ||
        WtoS == NULL || EtoS == NULL || EtoN == NULL || EtoW == NULL || NtoS == NULL || NtoW == NULL));

  cv_destroy(StoE);
  cv_destroy(StoW);
  cv_destroy(StoN);
  cv_destroy(WtoE);
  cv_destroy(NtoE);
  cv_destroy(WtoN);
  cv_destroy(WtoS);
  cv_destroy(NtoS);
  cv_destroy(NtoW);
  cv_destroy(EtoS);
  cv_destroy(EtoN);
  cv_destroy(EtoW);

  lock_destroy(intersectionLk);
  array_destroy(vehicles);
}



Vehicle* 
vehicle_create(Direction origin, Direction destination) {
  Vehicle *vehicle = kmalloc(sizeof(struct Vehicle));
  vehicle->origin = origin;
  vehicle->destination = destination;

  return vehicle;
}

bool 
detect_collisions(Vehicle *v1, Vehicle *v2) {
  if (v1->origin == v2->origin) {
      return false;
  } 
  else if (v1->origin == v2->destination && 
      		v2->origin == v1->destination) {
      return false;
  }
  else if (v1->destination != v2->destination &&
      	(right_turn(v1) || right_turn(v2))) {
      return false;
  }

  return true;
}


/*
 * The simulation driver will call this function each time a vehicle
 * tries to enter the intersection, before it enters.
 * This function should cause the calling simulation thread 
 * to block until it is OK for the vehicle to enter the intersection.
 *
 * parameters:
 *    * origin: the Direction from which the vehicle is arriving
 *    * destination: the Direction in which the vehicle is trying to go
 *
 * return value: none
 */

void
intersection_before_entry(Direction origin, Direction destination) 
{
    KASSERT(vehicles != NULL && intersectionLk != NULL);
    KASSERT(!(WtoN == NULL || WtoE == NULL || NtoE == NULL || StoE == NULL || StoN == NULL || StoW == NULL ||
    	WtoS == NULL || EtoS == NULL || EtoN == NULL || EtoW == NULL || NtoS == NULL || NtoW == NULL));

  lock_acquire(intersectionLk);
  Vehicle *newVec = vehicle_create(origin, destination);
  bool enter;

  while (true) {
      enter = true;
      for (unsigned int i=0; i<array_num(vehicles); ++i) {
	  Vehicle *waitVec = array_get(vehicles, i);
	  if (detect_collisions(newVec, waitVec)) {
	      if (origin == west && destination == east) {
		  cv_wait(WtoE, intersectionLk);
	      } else if (origin == west && destination == north) {
		  cv_wait(WtoN, intersectionLk);
	      } else if (origin == west && destination == south) {
		  cv_wait(WtoS, intersectionLk);
	      } else if (origin == east && destination == west) {
		  cv_wait(EtoW, intersectionLk);
	      } else if (origin == east && destination == north) {
		  cv_wait(EtoN, intersectionLk);
	      } else if (origin == east && destination == south) {
		  cv_wait(EtoS, intersectionLk);
	      } else if (origin == south && destination == north) {
		  cv_wait(StoN, intersectionLk);
	      } else if (origin == south && destination == west) {
		  cv_wait(StoW, intersectionLk);
	      } else if (origin == south && destination == east) {
		  cv_wait(StoE, intersectionLk);
	      } else if (origin == west && destination == east) {
		  cv_wait(WtoE, intersectionLk);
	      } else if (origin == west && destination == north) {
		  cv_wait(WtoN, intersectionLk);
	      } else if (origin == west && destination == south) {
		  cv_wait(WtoS, intersectionLk);
	      } else if (origin == north && destination == south) {
		  cv_wait(NtoS, intersectionLk);
	      } else if (origin == north && destination == west) {
		  cv_wait(NtoW, intersectionLk);
	      } else if (origin == north && destination == east) {
		  cv_wait(NtoE, intersectionLk);
	      }

	      enter = false;
	  }
      }

      if (enter) {
	  array_add(vehicles, newVec, NULL);
	  break;
      }

  }

  lock_release(intersectionLk);

  return;
}

/*
 * The simulation driver will call this function each time a vehicle
 * leaves the intersection.
 *
 * parameters:
 *    * origin: the Direction from which the vehicle arrived
 *    * destination: the Direction in which the vehicle is going
 *
 * return value: none
 */

void
intersection_after_exit(Direction origin, Direction destination) 
{
  KASSERT(!(WtoN == NULL || WtoE == NULL || NtoE == NULL || StoE == NULL || StoN == NULL || StoW == NULL ||
          WtoS == NULL || EtoS == NULL || EtoN == NULL || EtoW == NULL || NtoS == NULL || NtoW == NULL));
  KASSERT(vehicles != NULL && intersectionLk != NULL);

  lock_acquire(intersectionLk);
  
  for (unsigned int i=0; i<array_num(vehicles); ++i) {
       Vehicle *waitVec = array_get(vehicles, i);

       if (waitVec->origin == origin
	   && waitVec->destination == destination) {
	       array_remove(vehicles, i);
	   if (origin == west && destination == east) {
	       cv_broadcast(StoE, intersectionLk);
	       cv_broadcast(StoN, intersectionLk);
	       cv_broadcast(StoW, intersectionLk);
	       cv_broadcast(EtoS, intersectionLk);
	       cv_broadcast(NtoE, intersectionLk);
	       cv_broadcast(NtoS, intersectionLk);

	   } else if (origin == west && destination == south) {
	       cv_broadcast(EtoS, intersectionLk);
	       cv_broadcast(NtoS, intersectionLk);

	   } else if (origin == west && destination == north) {
	       cv_broadcast(StoN, intersectionLk);
	       cv_broadcast(StoW, intersectionLk);
	       cv_broadcast(NtoE, intersectionLk);
	       cv_broadcast(NtoS, intersectionLk);
	       cv_broadcast(EtoS, intersectionLk);
	       cv_broadcast(EtoN, intersectionLk);
	       cv_broadcast(EtoW, intersectionLk);

	   } else if (origin == east && destination == north) {
	       cv_broadcast(StoN, intersectionLk); 
	       cv_broadcast(WtoN, intersectionLk);      

	   } else if (origin == east && destination == south) {
	       cv_broadcast(StoN, intersectionLk);
	       cv_broadcast(StoW, intersectionLk);
	       cv_broadcast(NtoS, intersectionLk);
	       cv_broadcast(NtoE, intersectionLk);
	       cv_broadcast(WtoE, intersectionLk);
	       cv_broadcast(WtoS, intersectionLk);
	       cv_broadcast(WtoN, intersectionLk);

	   } else if (origin == east && destination == west) {
	       cv_broadcast(StoN, intersectionLk);
	       cv_broadcast(StoW, intersectionLk);
	       cv_broadcast(NtoS, intersectionLk);
	       cv_broadcast(NtoE, intersectionLk);
	       cv_broadcast(WtoN, intersectionLk);
	       cv_broadcast(NtoW, intersectionLk);

	   } else if (origin == south && destination == west) {
	       cv_broadcast(NtoE, intersectionLk);
	       cv_broadcast(NtoS, intersectionLk);
	       cv_broadcast(NtoW, intersectionLk);
	       cv_broadcast(WtoE, intersectionLk);
	       cv_broadcast(WtoN, intersectionLk);
	       cv_broadcast(EtoS, intersectionLk);
	       cv_broadcast(EtoW, intersectionLk);

	   } else if (origin == south && destination == north) {
	       cv_broadcast(WtoE, intersectionLk);
	       cv_broadcast(WtoN, intersectionLk);
	       cv_broadcast(NtoE, intersectionLk);
	       cv_broadcast(EtoS, intersectionLk);
	       cv_broadcast(EtoN, intersectionLk);
	       cv_broadcast(EtoW, intersectionLk);

	    } else if (origin == south && destination == east) {
		cv_broadcast(WtoE, intersectionLk);
		cv_broadcast(NtoE, intersectionLk);


	    } else if (origin == north && destination == east) {
		cv_broadcast(WtoE, intersectionLk);
		cv_broadcast(WtoN, intersectionLk);
		cv_broadcast(EtoS, intersectionLk);
		cv_broadcast(EtoW, intersectionLk);
		cv_broadcast(StoN, intersectionLk);
		cv_broadcast(StoW, intersectionLk);
		cv_broadcast(StoE, intersectionLk);

	    } else if (origin == north && destination == west) {
		cv_broadcast(StoW, intersectionLk);
		cv_broadcast(EtoW, intersectionLk);

	    } else if (origin == north && destination == south) {
		cv_broadcast(WtoE, intersectionLk);
		cv_broadcast(WtoN, intersectionLk);
		cv_broadcast(WtoS, intersectionLk);
		cv_broadcast(StoW, intersectionLk);
		cv_broadcast(EtoW, intersectionLk);
		cv_broadcast(EtoS, intersectionLk);
	    }
	    break;
       }
  }

  lock_release(intersectionLk);
}
